$('.uploadBtn').on('click', function() {
    $('#uploadFile').click();
});

$('#uploadFile').on('click', function() {
	// Necessary if same file is selected. If not set to null then 'change' event won't fire
	this.value = null;
});

$('#uploadFile').on('change', function(){

  var files = $(this).get(0).files;
  if (files.length > 0){
    var formData = new FormData();
    var file = files[0];
    formData.append('upload', file, file.name);
  	$.ajax({
		  url: '/fileUpload',
		  type: 'POST',
		  data: formData,
		  processData: false,
		  contentType: false,
		  success: function(data){
		      console.log('upload successful!');
		     	retrievMonthlyExpenses();
			},
			error: function(err) {
				console.log(err);
			}
		});  
	}
});

function retrievMonthlyExpenses() {
	$.ajax({
		  url: '/totalMonthlyExpense',
		  type: 'GET',
		  success: function(data){
		      console.log('upload successful!');
		      populateExpensesTable(data);
			},
			error: function(err) {
				console.log(err);
			}
		});  
}

function populateExpensesTable(expensesData) {
	$("#monthlyExpenses tbody").empty();	
	var count = 1;
	var expenseTable = $("#monthlyExpenses");
	for (var i = 0; i < expensesData.length; i++) {
		$("#monthlyExpenses tbody")
			.append($("<tr>")
				.append($("<td>")
					.append(count))
				.append($("<td>")
					.append(expensesData[i].year))
				.append($("<td>")
					.append(expensesData[i].month))
				.append($("<td>")
					.append(expensesData[i].totalExpenses)));
		count++;	
	}
}


