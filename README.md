Code Challenge:

===================INSTALLATION=================

For MacOSx

Only requirement prior to these steps is that Node.js is installed.

1. Clone repo through: git clone https://rvasanda@bitbucket.org/rvasanda/importdata.git OR simply use the extracted folder sent in email
2. Navigate to repo folder through Terminal
3. Run 'npm install'
4. Open config/config.json file and replace "localhost" with "159.203.14.171"
- This step ensures that you don't need to install mysql on your machine as the mysql-server is running remotely and is accessible to this app
5. Run 'npm start'
6. Navigate to localhost:3000 in browser
7. Upload .csv files (A sample file is provided, namely 'data_sample.csv')
8. To access database and verify persistence of expense records:
	- Connect through a database client (I use Sequel Pro)
	- Create a new connection in database client(user:root, password:root, database:importdata, host:159.203.14.171)
	- Connect and view created tables
9. On webpage (i.e. localhost:3000), after uploading a file, there should be a table showing total expenses grouped by year and month.	


===================GENERAL=================

I'm particularly happy with setting up and persisting all the data model classes correctly. I ran into plenty of problems using the 'sequelize' persistence library for Node.js but was able to work through all problems. 