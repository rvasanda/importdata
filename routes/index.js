var express = require('express');
var router = express.Router();
var models  = require('../models');
var lineReader = require('line-reader');
var async = require('async');
var fs = require('fs');
var sequelize = require('../models').sequelize;

var MOCK_FILE_NAME = "data_sample.csv";

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('main', { title: 'Express' });
});

router.post('/fileUpload', function(req, res, next) {
  console.log(req.uploadDirectory + req.uploadFileName);
  try {  
    var skipHeaderLine = true;
    var expenseRecordArray = [];
    
    lineReader.eachLine(req.uploadDirectory + req.uploadFileName, function(line, last) {
      console.log(line);

      if (skipHeaderLine) {
        skipHeaderLine = false;
      } else {
        // Split line by comma placements
        var flatDataArray = line.split(',');  

        var expenseRecord = {};
        expenseRecord.expenseDate = new Date(flatDataArray[0]);
        expenseRecord.category = flatDataArray[1];
        expenseRecord.firstName = flatDataArray[2];
        expenseRecord.lastName = flatDataArray[3];
        expenseRecord.address = {};
        expenseRecord.address.streetOne = flatDataArray[4];
        expenseRecord.address.city = flatDataArray[5];
        expenseRecord.address.state = flatDataArray[6];
        expenseRecord.address.postalCode = flatDataArray[7];        
        expenseRecord.expenseDescription = flatDataArray[8];
        expenseRecord.preTaxAmount = flatDataArray[9];
        expenseRecord.taxName = flatDataArray[10];
        expenseRecord.taxAmount = flatDataArray[11];

        // Cannot simply call persist operation here as multiple async calls will be issued at the same 
        // time, leading to unpredictable results (i.e. duplicate rows). Instead build array of lines first, 
        // then persist serially.  
        expenseRecordArray.push(expenseRecord);       
      }
      if(last){
        console.log("Done reading file. Now begin persisting.");

        async.forEachSeries(expenseRecordArray, persistExpenseRecord, function(err) {
          res.send("Done.");
        });

      }
    });  
  } catch(e) {
      console.log('Error:', e.stack);
  }
});

router.post('/importMockData', function(req, res, next) {
	// MOCK DATA: 
		// date, category, employee name, employee address, expense description, 
		// pre-tax amount, tax name, and tax amount
	var data = {};
	data.firstName = "John";
	data.lastName = "Smith";
	data.address = {};
	data.address.streetOne = "123 Fake Street";
	data.address.city = "Fake City";
	data.address.postalCode = "M6D N8J";
	data.address.state = "Ontario";
	data.address.country = "Canada";
	data.expenseDate = new Date();
	data.category = "New category";
	data.expenseDescription = "Description";
	data.preTaxAmount = 25.00;
	data.taxName = "HST";
	data.taxAmount = 3.25;

	try {
		persistExpenseRecord(data, res);	
	} catch (e) {
		console.log(e);
	}
 	
});

router.post('/importMockFile', function(req, res, next) {

  try {  
    var skipHeaderLine = true;
    var expenseRecordArray = [];
    
    lineReader.eachLine(MOCK_FILE_NAME, function(line, last) {
      console.log(line);

      if (skipHeaderLine) {
        skipHeaderLine = false;
      } else {
        // Split line by comma placements
        var flatDataArray = line.split(',');  

        var expenseRecord = {};
        expenseRecord.expenseDate = new Date(flatDataArray[0]);
        expenseRecord.category = flatDataArray[1];
        expenseRecord.firstName = flatDataArray[2];
        expenseRecord.lastName = flatDataArray[3];
        expenseRecord.address = {};
        expenseRecord.address.streetOne = flatDataArray[4];
        expenseRecord.address.city = flatDataArray[5];
        expenseRecord.address.state = flatDataArray[6];
        expenseRecord.address.postalCode = flatDataArray[7];        
        expenseRecord.expenseDescription = flatDataArray[8];
        expenseRecord.preTaxAmount = flatDataArray[9];
        expenseRecord.taxName = flatDataArray[10];
        expenseRecord.taxAmount = flatDataArray[11];

        // Cannot simply call persist operation here as multiple async calls will be issued at the same 
        // time, leading to unpredictable results (i.e. duplicate rows). Instead build array of lines first, then persist serially  
        expenseRecordArray.push(expenseRecord);  

        //persistExpenseRecordNew(expenseRecord, res);      
      }
      if(last){
        console.log("Done reading file. Now begin persisting.");

        async.forEachSeries(expenseRecordArray, persistExpenseRecord, function(err) {
          res.send("Done.");
        });

      }
    });  
  } catch(e) {
      console.log('Error:', e.stack);
  }
});

function persistExpenseRecord(data, callback) {
	
  // 1. Find or create Employee Record
  models.Employee.findOrCreate({
      where: {
      	firstName: data.firstName,
      	lastName: data.lastName
      }
  }).spread(function(employee, created) {

    if (employee) {
      console.log("Employee retrieved successfully ");

      // 2. Find or create Address Record
      models.Address.findOrCreate({	
        where: {
      		streetOne: data.address.streetOne,
      		city: data.address.city,
      		postalCode: data.address.postalCode,
      		state: data.address.state
      	}
      }).spread(function(employeeAddress, created) {
      	if (employeeAddress) {
      		console.log("Employee address retrieved successfully");      		
          employee.addAddress(employeeAddress);
      	} else {
      		console.log("Employee address could not be created or found!");		
      		callback(err);
      	}

      }).then(function() {
      	
      	// 3. Create new Expense Record 
      	models.Expense.create({
      		expenseDate: data.expenseDate,
      		description: data.expenseDescription,
      		preTaxAmount: data.preTaxAmount,
      		taxName: data.taxName,
      		taxAmount: data.taxAmount,      		
      	}).then(function(expense) {
      		expense.setEmployee(employee);
      		
      		// 4. Find or Create new Expense Category Record	
      		models.ExpenseCategory.findOrCreate({
      			where: {
      				category: data.category
      			}
      		}).spread(function(category, created) {

      			// 5. Link Expense to Category
      			expense.setExpenseCategory(category);
      			console.log("Expense Record created successfully!");
            callback();
      		});
      	});

      }); 


    } else {
      console.log("Employee could not be created or found successfully");
      res.status(500).render('error', { error: 'Employee could not be created or found successfully' });        
    }      

	});

router.get('/totalMonthlyExpense', function(req, res, next) {
  var queryString = "SELECT YEAR(expenseDate) as year, MONTHNAME(expenseDate) as month, (SUM(preTaxAmount)+SUM(taxAmount)) AS totalExpenses FROM expenses GROUP BY CAST(YEAR(expenseDate) AS CHAR(4)) + \'-\' + CAST(MONTH(expenseDate) AS CHAR(2))";
  sequelize.query(queryString, { type: sequelize.QueryTypes.SELECT}).then(function(queryResults) {
    res.send(queryResults);
  });
});

}  
module.exports = router;