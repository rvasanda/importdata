module.exports = function(sequelize, DataTypes) {
  var Address = sequelize.define("Address", {
    streetOne: DataTypes.CHAR,
    city: DataTypes.CHAR,
    state: DataTypes.CHAR,
    postalCode: DataTypes.CHAR
  },
  {
  	classMethods: {
  		associate: function(models) {
  			Address.belongsToMany(models.Employee, { through: 'EmployeeAddress'});
  		}
  	}	
  });
  return Address;
};