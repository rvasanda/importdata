module.exports = function(sequelize, DataTypes) {
  var Employee = sequelize.define("Employee", {
    firstName: DataTypes.CHAR,
    lastName: DataTypes.CHAR
  },
  {
  	classMethods: {
  		associate: function(models) {
  			Employee.belongsToMany(models.Address, {through: 'EmployeeAddress' });
        Employee.hasMany(models.Expense);
  		}
  	}	
  });
  return Employee;
};