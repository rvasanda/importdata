module.exports = function(sequelize, DataTypes) {
  var ExpenseCategory = sequelize.define("ExpenseCategory", {
    category: DataTypes.CHAR
  });
  return ExpenseCategory;
};