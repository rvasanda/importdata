module.exports = function(sequelize, DataTypes) {
  var Expense = sequelize.define("Expense", {
    expenseDate: DataTypes.DATE,
    description: DataTypes.TEXT,
    preTaxAmount: DataTypes.DECIMAL(10,2),
    taxName: DataTypes.CHAR,
    taxAmount: DataTypes.DECIMAL(10,2)
  },    
  {
  	classMethods: {
  		associate: function(models) {
  			Expense.belongsTo(models.ExpenseCategory);
        Expense.belongsTo(models.Employee);
  		}
  	}	
  });
  return Expense;
};